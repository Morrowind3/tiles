package client.controller;

import client.view.AppFrame;
import core.factory.PuzzleBlockFactory;
import core.loading.FilePuzzleBlockShapeLoader;
import core.model.Game;

import java.util.Random;

public class AppController {

	private AppFrame view;
	private Game game;
	private final FilePuzzleBlockShapeLoader puzzleBlockShapeLoader;
	private GameController gameController;

	private AppController() {
		puzzleBlockShapeLoader = new FilePuzzleBlockShapeLoader(game);
		puzzleBlockShapeLoader.load();

		PuzzleBlockFactory puzzleBlockFactory = new PuzzleBlockFactory(puzzleBlockShapeLoader);

		game = new Game(puzzleBlockFactory);
		gameController = new GameController(game);
		view = new AppFrame(gameController);
		view.setVisible(true);
		gameController.start(view, false);

	}

	public static void main(String[] args) {

		new AppController();
	}

}