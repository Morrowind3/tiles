package client.controller;

import client.view.GameOverPanel;
import client.view.GamePanel;
import core.model.*;

import javax.swing.*;
import java.awt.*;

public class GameController {

	private Game game;
	private GamePanel view;
	private JFrame thisFrame;

	public GameController(Game game) {

		this.game = game;
	}

	/*
	 * Arranges everything to start a game. if it's a 'new game' (i.e. One that
	 * doesn't continue from a previous session) it takes a little extra step to
	 * ensure the grid is empty.
	 */
	public void start(JFrame frame, boolean newGame) {

		thisFrame = frame;
		if (newGame) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					game.getGrid().getGridState()[x][y] = null;
				}
			}
			game.restockDocks();
			game.clearCurrentScore();
			view.updateCurrentScore(0);
			view.refreshGrid(game.getGrid());
			view.showDockedBlocks(game.getDockedBlocks());
		} else {

			game.loadGame();
			setScore(game.getCurrentScore(), game.getHighScore());

			view.showDockedBlocks(game.getDockedBlocks());
			view.refreshGrid(game.getGrid());
		}
	}

	/*
	 * The view used by this controller is set inside that actual view (gamePanel in
	 * this case). Theoretically, this will allow making a new view without ever
	 * having to touch the controller.
	 */
	public void setView(GamePanel view) {
		this.view = view;
	}

	public void placePuzzleBlock(PuzzleBlock puzzleBlock, Point pos) {

		try {
			game.placeBlock(puzzleBlock, pos);
		} catch (ShapeOutOfBoundsException | ShapeOverlapException e) {
			view.showInvalidPlacementError();
		}
		if (!game.HasNotLost()) {
			openGameOverPanel();
		}
		view.updateCurrentScore(game.getCurrentScore());
		view.updateHighScore(game.getHighScore());
		view.showDockedBlocks(game.getDockedBlocks());
		view.refreshGrid(game.getGrid());
	}

	public void setScore(int current, int best) {
		view.updateCurrentScore(current);
		view.updateHighScore(best);
	}

	public void saveGame() {
		game.saveGame();
	}

	public void cheat() {
		game.clearCurrentScore();
		game.restockDocks();
		view.updateCurrentScore(game.getCurrentScore());
		view.showDockedBlocks(game.getDockedBlocks());
	}

	public boolean gameNotLost() {
		return game.HasNotLost();
	}

	public void openGameOverPanel() {
		thisFrame.setGlassPane(new GameOverPanel(this, thisFrame));
		thisFrame.getGlassPane().setVisible(true);
	}

	public boolean gotHighScore() {
		if (game.getHighScore() == game.getCurrentScore()) {
			return true;
		} else
			return false;

	}

	public int getCurrentScore() {
		return game.getCurrentScore();
	};
}
