package client.view;


import javax.swing.*;

import client.controller.GameController;
import core.utility.ComponentListenerAdapter;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


public class AppFrame extends JFrame {

    private GamePanel gamePanel;
    private GameController gameController;
    
    public AppFrame(GameController gameController) {
    	this.gameController = gameController;
        setTitle("TILES! door Harley Rombout");
        setSize(500, 700);
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        
        setResizable(false);
        setLocationRelativeTo(null);
        gamePanel = new GamePanel(gameController);
        add(gamePanel);

        WindowListener exitListener = new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e) {
            		if(gameController.gameNotLost()) {
                    gameController.saveGame();
            		} else {
            			saveCleanSlate();		//prevents saving a lost/finished game.
            		}
                    System.exit(0);
            }
        };
        addWindowListener(exitListener);
    }

     protected void saveCleanSlate() {
    	gameController.start(this, true);
    	gameController.saveGame();
    }
    
}


