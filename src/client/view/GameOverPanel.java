package client.view;


import javax.swing.*;

import client.controller.GameController;
import javafx.geometry.Rectangle2D;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

public class GameOverPanel extends JPanel implements MouseListener {


private JButton cancel;
private JButton replay;

private Rectangle2D goBackButton = new Rectangle2D(90,310,140, 120);
private Rectangle2D restartButton = new Rectangle2D(250,310,140, 120);
private GameController controller;
private JFrame thisFrame;
private JLabel gameOverLabel = new JLabel();
private File cupImage= new File("resource\\imgs\\cup.png");

public GameOverPanel(GameController controller, JFrame frame){
	this.controller = controller;
	thisFrame = frame;
    setBackground(new Color(200,200,200,20));
    setOpaque(false);
    setLayout(null);
    repaint();
    revalidate();
    addMouseListener(this);

    if(controller.gameNotLost()) {
    	gameOverLabel.setText("You surrendered!");
    } else gameOverLabel.setText((" No moves left!"));
    gameOverLabel.setFont(new Font("Tahoma", Font.BOLD,20));
    gameOverLabel.setForeground(Color.white);
    gameOverLabel.setBounds(100,40,300,300);
    add(gameOverLabel);
    
    JLabel cup = new JLabel();
    cup.setBounds(190,150,200,200);
    cup.setFont(new Font("Tahoma", Font.BOLD,20));
    cup.setForeground(Color.white);
    cup.setText("" + controller.getCurrentScore());
    if(controller.gotHighScore()) {
    cup.setIcon(new ImageIcon(cupImage.toString()));
    }
    add(cup);
    
    JLabel goBack = new JLabel("Go back");
    goBack.setForeground(Color.WHITE);
    goBack.setBounds(100, 200, 250, 250);
    if(!controller.gameNotLost()) {
    	goBack.setText("<HTML> There's no going<br> back for losers.</HTML>");
    }
    add(goBack);
    JLabel restart = new JLabel("Restart");
    restart.setForeground(Color.WHITE);
    restart.setBounds(300, 200, 250, 250);
    add(restart);

}

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(new Color(255, 255, 255, 140));
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.blue);
        g.fillRoundRect(90,150,300,150, 20,20);
        g.setColor(Color.green);
        g.fillRoundRect(90,310,140, 120,20,20);
        g.setColor(Color.orange);
        g.fillRoundRect(250,310,140, 120,20,20);
    }


    @Override
    public void mouseClicked(MouseEvent e) {
    	e.consume();
    	Point clickLocation = new Point(e.getX(), e.getY());
    	if(pointIntersects(goBackButton, clickLocation)) {
    		if(controller.gameNotLost()){
    		setVisible(false);
    		}
    	};
    	if(pointIntersects(restartButton, clickLocation)) {
    		controller.start(thisFrame, true);
    		setVisible(false);
    	}
        
    }
   
    

    @Override
    public void mousePressed(MouseEvent e) {
        e.consume();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        e.consume();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        e.consume();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        e.consume();
    }
    
    private boolean pointIntersects(Rectangle2D button, Point point) {
    	if(point.x > button.getMinX() && point.x < button.getMaxX() && point.y > button.getMinY() && point.y < button.getMaxY()) {
    		return true;
    	} else	return false;
    }
}
