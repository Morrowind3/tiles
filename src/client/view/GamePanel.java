package client.view;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.border.Border;

import client.controller.GameController;
import core.model.Grid;
import core.model.PuzzleBlock;
import core.model.PuzzleBlockShape;
import core.utility.ComponentListenerAdapter;
import core.utility.MouseListenerAdapter;

public class GamePanel extends JPanel {

	
	// cell appearance  constants.
    private final static Border DEFAULT_BORDER = BorderFactory.createLineBorder(new Color(230, 230, 230), 1);
    private final static Border HOVER_BORDER_OCCUPIED = BorderFactory.createLineBorder(new Color(255, 0, 0), 3);
    private final static Border BLOCK_BORDER = BorderFactory.createRaisedBevelBorder();
    private final static Border HOVER_BORDER_CLEAR = BorderFactory.createLineBorder(new Color(0, 255, 0), 3);
    private final static Color DEFAULT_COLOR = new Color(200,200,200);


    private GameController gameController;


    private JButton[][] playingGrid = new JButton[10][10];
    private Map<JButton, Point> buttonCoordinates = new HashMap<>();

    private PuzzleBlock draggedPuzzleBlock;
    private JButton hoveredPuzzleBlock;

    private PuzzleBlockView dockView1;
    private PuzzleBlockView dockView2;
    private PuzzleBlockView dockView3;

    private ScoreView scoreView;

    public GamePanel(GameController controller) {
        gameController = controller;
        gameController.setView(this);
        initializeControls();
    }

    private void initializeControls() {

        BorderLayout borderLayout = new BorderLayout();
        setLayout(borderLayout);
        createScorePanel();
        createGridPanel();
        createDockPanel();

    }



    private void createScorePanel() { ;
        scoreView = new ScoreView(gameController);
        add(scoreView, BorderLayout.PAGE_START);

    }

    private void createGridPanel() {
        JPanel gridPanel = new JPanel();
        gridPanel.setBackground(Color.decode("#89aee1"));
        add(gridPanel, BorderLayout.CENTER);
        JPanel playingGrid = createPlayingGrid();
        gridPanel.add(playingGrid);
    }

    private JPanel createDockPanel() {
        JPanel dockPanel = new JPanel();
        BoxLayout layout = new BoxLayout(dockPanel, BoxLayout.LINE_AXIS);
        dockPanel.setLayout(layout);

        Dimension size = dockPanel.getPreferredSize();
        size.height = 180;
        dockPanel.setPreferredSize(size);
        add(dockPanel, BorderLayout.PAGE_END);

        dockView1 = new PuzzleBlockView();
        dockPanel.add(dockView1);

        dockView2 = new PuzzleBlockView();
        dockPanel.add(dockView2);

        dockView3 = new PuzzleBlockView();
        dockPanel.add(dockView3);

        return dockPanel;
    }

    private JPanel createPlayingGrid() {
        JPanel panel = new JPanel();
        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);

        GridBagConstraints constraints = new GridBagConstraints();
        layout.setConstraints(panel, constraints);

        for(int y = 0; y < 10; y++) {
            for(int x = 0; x < 10; x++) {
                JButton button = new JButton();
                button.setPreferredSize(new Dimension(35, 35));
                button.setEnabled(false);
                button.setBorder(DEFAULT_BORDER);
                button.setBackground(DEFAULT_COLOR);
                button.addMouseListener(new MouseListenerAdapter() {
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        clearGrid();

                        if(draggedPuzzleBlock != null) {
                            hoveredPuzzleBlock = (JButton) e.getComponent();
                            PuzzleBlockShape shape = draggedPuzzleBlock.getShape();
                            char[][] shapeData = shape.getBlocks();

                            Point hoverPos = buttonCoordinates.get(e.getComponent());
                            for(int y = 0; y < 5; y++) {
                                for(int x = 0; x < 5; x++) {
                                    try {
                                        if(shapeData[y][x] != '.') {
                                            if(playingGrid[hoverPos.y + y][hoverPos.x + x].getBorder() == BLOCK_BORDER){
                                                playingGrid[hoverPos.y + y][hoverPos.x + x].setBorder(HOVER_BORDER_OCCUPIED);
                                        } else playingGrid[hoverPos.y + y][hoverPos.x + x].setBorder(HOVER_BORDER_CLEAR);
                                        }
                                    } catch(Exception ec) {
                                    }
                                }
                            }
                        }
                    }
                });


                playingGrid[y][x] = button;
                buttonCoordinates.put(button, new Point(x, y));

                constraints.gridx = x;
                constraints.gridy = y;
                panel.add(button, constraints);
            }
        }

        return panel;
    }

    //called every time you enter a new cell. Handles the 'shape' ghost.
    private void clearGrid() {
        for (JButton entry : buttonCoordinates.keySet()) {


            if (entry.getBorder() == HOVER_BORDER_CLEAR) {
                entry.setBorder(DEFAULT_BORDER);
            }
            if (entry.getBorder() == HOVER_BORDER_OCCUPIED) {
                entry.setBorder(BLOCK_BORDER);
            }

        }
    }

    public void showDockedBlocks(List<PuzzleBlock> dockedBlocks) {
        PuzzleBlock puzzleBlock1 = dockedBlocks.get(0);
        dockView1.setPuzzleBlock(puzzleBlock1);
        dockView1.addMouseListener(new PuzzleBlockViewMouseListener(dockView1));

        PuzzleBlock puzzleBlock2 = dockedBlocks.get(1);
        dockView2.setPuzzleBlock(puzzleBlock2);
        dockView2.addMouseListener(new PuzzleBlockViewMouseListener(dockView2));

        PuzzleBlock puzzleBlock3 = dockedBlocks.get(2);
        dockView3.setPuzzleBlock(puzzleBlock3);
        dockView3.addMouseListener(new PuzzleBlockViewMouseListener(dockView3));
    }

    
    private final class PuzzleBlockViewMouseListener extends MouseListenerAdapter {
        private PuzzleBlockView view;

        public PuzzleBlockViewMouseListener(PuzzleBlockView view) {
            this.view = view;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            draggedPuzzleBlock = view.getPuzzleBlock();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if(hoveredPuzzleBlock != null){


                gameController.placePuzzleBlock(draggedPuzzleBlock, buttonCoordinates.get(hoveredPuzzleBlock));;

                draggedPuzzleBlock = null;
                hoveredPuzzleBlock = null;
            }

        }
    }

    public void refreshGrid(Grid grid) {
        clearGrid();

        for(int y = 0; y < 10; y++) {
            for(int x = 0; x < 10; x++) {
                PuzzleBlock block = grid.getPuzzleBlock(new Point(x, y));
                if(block != null) {
                    playingGrid[y][x].setBackground(block.getShape().getColour());
                    playingGrid[y][x].setBorder(BLOCK_BORDER);
                } else{
                    playingGrid[y][x].setBackground(DEFAULT_COLOR);
                    playingGrid[y][x].setBorder(DEFAULT_BORDER);
                }
                }
            }
        }


    public void showInvalidPlacementError(){
        System.out.println("Invalid placement!");

    }

    public void updateCurrentScore(int points){

        scoreView.updateCurrentScore(points);
    }
    public void updateHighScore(int points){
        scoreView.updateHighScore(points);
    }

}
