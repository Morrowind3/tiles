package client.view;

import core.model.PuzzleBlock;
import core.model.PuzzleBlockShape;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseListener;

public class PuzzleBlockView extends JPanel {
	
	//constant for a cell's default (unfilled) border
    private static final Border DEFAULT_BORDER = BorderFactory.createLineBorder(new Color(200, 200, 200), 1);

    private PuzzleBlock puzzleBlock;

    private GridBagConstraints constraints;

    private JButton[][] cells;

    public PuzzleBlockView() {
        this.cells = new JButton[5][5];
        setBackground(Color.decode("#89aee1"));
        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);

        constraints = new GridBagConstraints();
        layout.setConstraints(this, constraints);

        createEmptyDock(constraints);
    }

    public PuzzleBlock getPuzzleBlock() {
        return puzzleBlock;
    }

    public void setPuzzleBlock(PuzzleBlock block) {

        clearDock();

        this.puzzleBlock = block;
        if(block == null) {

            return;
        }

        PuzzleBlockShape shape = puzzleBlock.getShape();
        char[][] shapeData = shape.getBlocks();
        for(int y = 0; y < 5; y++) {
            for(int x = 0; x < 5; x++) {
                if(shapeData[y][x] != '.') {
                    cells[y][x].setBackground(shape.getColour());
                    cells[y][x].setBorder(BorderFactory.createRaisedBevelBorder());
                    cells[y][x].revalidate();
                }
            }
        }
    }

    private void clearDock() {
        puzzleBlock = null;

        for(int y = 0; y < 5; y++) {
            for(int x = 0; x < 5; x++) {
                cells[y][x].setBackground(Color.WHITE);
                cells[y][x].setBorder(DEFAULT_BORDER);
            }
        }
    }

    private void createEmptyDock(GridBagConstraints constraints){
        for(int y = 0; y < 5; y++) {
            for(int x = 0; x < 5; x++) {
                JButton button = new JButton();
                button.setPreferredSize(new Dimension(25, 25));
                button.setEnabled(false);

                constraints.gridx = x;
                constraints.gridy = y;
                add(button, constraints);

                cells[y][x] = button;
            }
        }
    }

    @Override
    public synchronized void addMouseListener(MouseListener l) {
        if(puzzleBlock == null)
            return;

        for(int y = 0; y < 5; y++) {
            for(int x = 0; x < 5; x++) {
                    cells[y][x].addMouseListener(l);
            }
        }
    }
}
