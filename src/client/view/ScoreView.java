package client.view;

import client.controller.GameController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class ScoreView extends JPanel {



    private JLabel currentScoreLabelNumber;
    private JLabel highSScoreLabelNumber;
    private File cupImage= new File("resource\\imgs\\cup.png");
    private Font LabelFont = new Font( "Old English Text MT", Font.PLAIN,24);
    private GridBagConstraints constraints;
    private GameController controller;

    ScoreView(GameController controller){
        this.controller = controller;
        setBackground(Color.decode("#89aee1"));
        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);
        constraints = new GridBagConstraints();
        layout.setConstraints(this, constraints);

        Header();


    }

    private void Header(){

        //current Score
        JLabel currentScoreLabelText = new JLabel("Current score:");
        constraints.gridx = 2;

        currentScoreLabelText.setFont(LabelFont);
        add(currentScoreLabelText,constraints);
        currentScoreLabelNumber = new JLabel("0");
        constraints.gridy = 1;
        currentScoreLabelNumber.setFont(LabelFont);
        add(currentScoreLabelNumber, constraints);

        //cup
        constraints.gridx = 3;
        JLabel cupLabel = new JLabel();
        cupLabel.setIcon(new ImageIcon(cupImage.toString()));
        add(cupLabel, constraints);
        
        //highest Score
        JLabel highScoreLabelText = new JLabel("Highest score:");
        constraints.gridy = 0;
        constraints.gridx = 4;
        highScoreLabelText.setFont(LabelFont);
        add(highScoreLabelText,constraints);
        highSScoreLabelNumber = new JLabel("0");
        constraints.gridy = 1;
        highSScoreLabelNumber.setFont(LabelFont);
        add(highSScoreLabelNumber, constraints);

        //Surrender button
        JButton surrenderButton = new JButton("Give up!");
        constraints.gridx = 10;
        constraints.gridy = 1;
        surrenderButton.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        add(surrenderButton, constraints);
        surrenderButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
			controller.openGameOverPanel();
				
			}
        	
        });
        //Cheat button
        JButton cheatButton = new JButton("<HTML>C<br>h<br>e<br>a<br>t<br>!</HTML>");
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridheight =3;
        cheatButton.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        add(cheatButton, constraints);
        cheatButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				controller.cheat();
			}
        	
        });


    }

    public void updateCurrentScore(int points){
        currentScoreLabelNumber.setText("" + points);
    }
    public void updateHighScore(int points){
        highSScoreLabelNumber.setText("" + points);
    }

}
