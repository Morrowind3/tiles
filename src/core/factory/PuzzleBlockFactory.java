package core.factory;

import java.util.Random;

import core.loading.FilePuzzleBlockShapeLoader;
import core.model.PuzzleBlock;
import core.model.PuzzleBlockShape;

//Get your fresh blocks here 

public class PuzzleBlockFactory {

	private final Random random;
	private final FilePuzzleBlockShapeLoader loader;

	public PuzzleBlockFactory(FilePuzzleBlockShapeLoader loader) {
		this.random = new Random();
		this.loader = loader;
	}

	public PuzzleBlock getNewRandomBlock() {
		int randomShapeId = random.nextInt(19);
		PuzzleBlockShape shape = loader.getShape(randomShapeId);
		return new PuzzleBlock(shape);
	}

	public PuzzleBlock getNewBlock(int shape) {
		return new PuzzleBlock(loader.getShape(shape));
	}

}