package core.loading;

import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import core.model.Game;
import core.model.PuzzleBlockShape;
import core.utility.FileHandler;

/*This class creates every shape after starting, and assigns all the requires data. 
 * 
 */

public class FilePuzzleBlockShapeLoader {

	private final FileHandler shapeFile;

	private final Map<Integer, PuzzleBlockShape> shapes = new HashMap<>();

	private char[][] dummyShape = new char[5][5];

	private File blockDefine = new File("resource\\configs\\blocks.txt");

	public FilePuzzleBlockShapeLoader(Game game) {
		shapeFile = new FileHandler(blockDefine);
	}

	public void load() {
		for (int i = 0; i < 19; i++) {
			String blockDef = shapeFile.getLine(i);
			char[][] shape = defineShape(blockDef);
			shapes.put(i, new PuzzleBlockShape(shape, setColour(i), setIdentifier(i)));
		}
		// empty dummy shape for empty spaces
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				dummyShape[y][x] = '.';

			}
		}
		shapes.put(20, new PuzzleBlockShape(dummyShape, setColour(20), setIdentifier('X')));

	}

	public PuzzleBlockShape getShape(int shapeId) {
		if (!shapes.containsKey(shapeId)) {
			throw new IndexOutOfBoundsException(String.format("Shape with ID '%s' can not be found", shapeId));
		}
		return shapes.get(shapeId);
	}

	private char[][] defineShape(final String blockDef) {
		char[][] block = new char[5][5];

		for (int x = 0, first = 0, last = 5; x < 5; x++, first = first + 5, last = last + 5) {
			block[x] = blockDef.substring(first, last).toCharArray();
		}

		return block;
	}

	private Color setColour(int blockNum) {

		switch (blockNum) {
		case 0:
			return new Color(122, 130, 207);
		case 1:
			return new Color(244, 199, 62);
		case 2:
			return new Color(244, 199, 62);
		case 3:
			return new Color(219, 140, 69);
		case 4:
			return new Color(219, 140, 69);
		case 5:
			return new Color(105, 219, 126);
		case 6:
			return new Color(105, 219, 126);
		case 7:
			return new Color(105, 219, 126);
		case 8:
			return new Color(105, 219, 126);
		case 9:
			return new Color(211, 82, 122);
		case 10:
			return new Color(211, 82, 122);
		case 11:
			return new Color(157, 235, 81);
		case 12:
			return new Color(199, 82, 79);
		case 13:
			return new Color(199, 82, 79);
		case 14:
			return new Color(104, 193, 224);
		case 15:
			return new Color(104, 193, 224);
		case 16:
			return new Color(104, 193, 224);
		case 17:
			return new Color(104, 193, 224);
		case 18:
			return new Color(97, 229, 172);
		default:
			return Color.black;
		}

	}

	// these identifiers are used by the savefiles but don't really serve a purpose
	// inside the application.
	private char setIdentifier(int blockNum) {

		switch (blockNum) {
		case 0:
			return 'A';
		case 1:
			return 'B';
		case 2:
			return 'C';
		case 3:
			return 'D';
		case 4:
			return 'E';
		case 5:
			return 'F';
		case 6:
			return 'G';
		case 7:
			return 'H';
		case 8:
			return 'I';
		case 9:
			return 'J';
		case 10:
			return 'K';
		case 11:
			return 'L';
		case 12:
			return 'M';
		case 13:
			return 'N';
		case 14:
			return 'O';
		case 15:
			return 'P';
		case 16:
			return 'Q';
		case 17:
			return 'R';
		case 18:
			return 'S';
		default:
			return '.';
		}

	}
}