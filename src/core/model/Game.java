package core.model;

import java.awt.Point;
import java.io.File;
import java.util.*;

import core.factory.PuzzleBlockFactory;
import core.utility.FileHandler;
import core.utility.FileWriter;
import core.utility.SoundPlayer;

public class Game {

	private final PuzzleBlockFactory puzzleBlockFactory;
	private File saveGame = new File("resource\\configs\\savegame.txt");
	private final FileHandler saveReader;
	private FileWriter fileWriter;

	private int currentScore;
	private int highScore;

	private final Grid grid;
	private final Map<Integer, PuzzleBlock> docked;

	public Game(final PuzzleBlockFactory PuzzleBlockFactory) {
		this.puzzleBlockFactory = PuzzleBlockFactory;
		this.grid = new Grid();
		this.docked = new HashMap<>(3);

		saveReader = new FileHandler(saveGame);
	}

	public void restockDocks() {
		docked.clear();
		for (int i = 0; i < 3; i++) {
			PuzzleBlock block = puzzleBlockFactory.getNewRandomBlock();
			docked.put(i, block);

		}
	}

	public List<PuzzleBlock> getDockedBlocks() {
		List<PuzzleBlock> dockedBlocks = new ArrayList<>();

		dockedBlocks.add(getDockedBlock(0));
		dockedBlocks.add(getDockedBlock(1));
		dockedBlocks.add(getDockedBlock(2));

		return dockedBlocks;
	}

	public PuzzleBlock getDockedBlock(int index) {
		return docked.get(index);
	}

	public void placeBlock(PuzzleBlock block, Point pos) {
		grid.placeBlock(block, pos);
		clearDockedBlock(block);

		if (getDockedBlocks().get(0) == null && getDockedBlocks().get(1) == null && getDockedBlocks().get(2) == null) {
			restockDocks();
		}
		HasNotLost();
		addToCurrentScore(block.getShape().amountOfBlocks());
		processLineMatches(block);
		if (currentScore > highScore) {
			highScore = currentScore;

		}

	}

	private void clearDockedBlock(PuzzleBlock block) {
		Integer blockIndex = null;
		for (Map.Entry<Integer, PuzzleBlock> dockedBlockEntry : docked.entrySet()) {
			if (dockedBlockEntry.getValue() == block) {
				blockIndex = dockedBlockEntry.getKey();
			}
		}
		if (blockIndex != null) {
			docked.put(blockIndex, null);
		}
	}

	/**
	 * Checks whether the player has scored a line, removes that line from play and
	 * adds points to the score.
	 */
	public void processLineMatches(PuzzleBlock lastPlacedBlock) {

		int linesCleared = 0;

		int horizontalBlocks = 0;
		// horizontal lines
		for (int y = 0; y < 10; y++) {
			horizontalBlocks = 0;
			for (int x = 0; x < 10; x++) {
				if (grid.getGridState()[y][x] != null) {
					horizontalBlocks++;
				}
				if (horizontalBlocks == 10) {
					grid.clearLine(y, true);
					linesCleared++;

				}
			}
		}
		// vertical lines
		int verticalBlocks = 0;
		for (int x = 0; x < 10; x++) {
			verticalBlocks = 0;
			for (int y = 0; y < 10; y++) {
				if (grid.getGridState()[y][x] != null) {
					verticalBlocks++;
				}
				if (verticalBlocks == 10) {
					grid.clearLine(x, false);
					linesCleared++;
				}
			}
		}
		if (linesCleared > 0) {
			calculateScore(linesCleared);
		}

	}

	private void calculateScore(int linesCleared) {
		/**
		 * a flat 10 points per row/column cleared.
		 */
		addToCurrentScore(10 * linesCleared);

		/**
		 * scaled Bonus points for clearing several lines at once
		 */
		switch (linesCleared) {
		case 2: {
			addToCurrentScore(10);
			break;
		}
		case 3: {
			addToCurrentScore(30);
			break;
		}
		case 4: {
			addToCurrentScore(60);
			break;
		}
		case 5: {
			addToCurrentScore(100);
			break;
		}
		case 6: {
			addToCurrentScore(150);
			break;
		}
		default:
			break;
		}

	}

	/**
	 * Checks whether the player has lost the game. The player has lost the game if
	 * the player surrenders or can not make a new move.
	 */
	public boolean HasNotLost() {

		Point cellBeingChecked = new Point(0, 0);
		List<PuzzleBlock> dockedBlocks = getDockedBlocks();
		char[][] shapeData;
		boolean possibleMove = false;

		// This is a pretty complex nest of loops, I'll add some short note for each phase.

		while (cellBeingChecked.getY() <= 10 && !possibleMove) {		//loop stops if either every cell is checked, or a possible move has been found (so no point in checking further)
			for (int blockBeingChecked = 0; blockBeingChecked < 3; blockBeingChecked++) {		//for every dock
				if (dockedBlocks.get(blockBeingChecked) == null) {
					continue;		//skip this dock if it's empty
				}
				shapeData = dockedBlocks.get(blockBeingChecked).getShape().getBlocks();		//put the shape of the current dock in an array
				for (int y = 0; y < shapeData.length; y++) {
					for (int x = 0; x < shapeData[y].length; x++) {
						try {
							if (grid.canPlace(getDockedBlock(blockBeingChecked), cellBeingChecked)) {		
								possibleMove = true;
							}
							;
						} catch (ShapeOverlapException | ShapeOutOfBoundsException e) {
							// These exception are only relevant for actual placement.
						}
					}
				}

			}
			//Move on to the next cell
			if (cellBeingChecked.getX() == 10) {
				cellBeingChecked.y++;
				cellBeingChecked.x = 0;
			} else
				cellBeingChecked.x++;
		}
		return possibleMove;
	}

	public Grid getGrid() {
		return grid;
	}

	public int getCurrentScore() {
		return currentScore;
	}

	public int getHighScore() {
		return highScore;
	}

	public void addToCurrentScore(int points) {
		currentScore = currentScore + points;
	}

	public void saveGame() {
		fileWriter = new FileWriter(saveGame.toString());
		// board
		for (int y = 0; y < 10; y++) {

			for (int x = 0; x < 10; x++) {
				fileWriter.writeLine(
						String.valueOf(getGrid().getGridState()[y][x] != null ? getGrid().getGridState()[y][x] : '.'));
			}
			fileWriter.newLine();
			// blocks in docks
		}
		for (int i = 0; i < 3; i++) {
			if (getDockedBlock(i) != null) {
				fileWriter
						.writeLine(Character.getNumericValue(getDockedBlock(i).getShape().getIdentifier()) - 10 + " ");
			} else
				fileWriter.writeLine("20 ");
		}

		// scores
		fileWriter.newLine();
		fileWriter.writeLine(("" + currentScore));

		fileWriter.newLine();
		fileWriter.writeLine(("" + highScore));

	}

	public void loadGame() {
		if (saveGame.exists()) {
			importGridState();
			placeInDock();
			currentScore = Integer.parseInt(saveReader.getLine(11));
			highScore = Integer.parseInt(saveReader.getLine(12));
		} else {
			restockDocks();
		}

	}

	public void clearCurrentScore() {
		currentScore = 0;

	}

	private void placeInDock() {
		docked.clear();
		String[] stringHolder = saveReader.getLine(10).split(" ");
		;
		for (int i = 0; i < 3; i++) {
			if (stringHolder[i].equals("20")) {
				docked.put(i, null);

			} else
				docked.put(i, puzzleBlockFactory.getNewBlock(Integer.parseInt(stringHolder[i])));
		}

	}

	private void importGridState() {
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				switch (saveReader.getLine(y).charAt(x)) {
				case 'A':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(0), new Point(y, x));
					break;
				case 'B':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(1), new Point(y, x));
					break;
				case 'C':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(2), new Point(y, x));
					break;
				case 'D':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(3), new Point(y, x));
					break;
				case 'E':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(4), new Point(y, x));
					break;
				case 'F':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(5), new Point(y, x));
					break;
				case 'G':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(6), new Point(y, x));
					break;
				case 'H':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(7), new Point(y, x));
					break;
				case 'I':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(8), new Point(y, x));
					break;
				case 'J':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(9), new Point(y, x));
					break;
				case 'K':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(10), new Point(y, x));
					break;
				case 'L':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(11), new Point(y, x));
					break;
				case 'M':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(12), new Point(y, x));
					break;
				case 'N':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(13), new Point(y, x));
					break;
				case 'O':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(14), new Point(y, x));
					break;
				case 'P':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(15), new Point(y, x));
					break;
				case 'Q':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(16), new Point(y, x));
					break;
				case 'R':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(17), new Point(y, x));
					break;
				case 'S':
					grid.placeBlockPiece(puzzleBlockFactory.getNewBlock(18), new Point(y, x));
					break;
				default:
					break;
				}
			}
		}
	}
}
