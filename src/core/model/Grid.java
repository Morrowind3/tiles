package core.model;

import java.awt.Point;
import java.io.File;

import core.utility.SoundPlayer;

public class Grid {

	private final int WIDTH = 10;
	private final int HEIGHT = 10;
	private File clearLineSound = new File("resource\\audio\\woosh.wav");
	private final SoundPlayer clearLineSoundPlayer = new SoundPlayer(clearLineSound);
	private PuzzleBlock[][] grid;

	public Grid() {
		grid = new PuzzleBlock[WIDTH][HEIGHT];
	}

	public boolean canPlace(PuzzleBlock block, Point pos) {
		PuzzleBlockShape shape = block.getShape();
		char[][] shapeData = shape.getBlocks();

		for (int y = 0; y < shapeData.length; y++) {
			for (int x = 0; x < shapeData[y].length; x++) {
				if (isShapeBlock(shapeData[y][x])) {
					try {
						PuzzleBlock shapeAtTarget = grid[pos.y + y][pos.x + x];
						if (shapeAtTarget != null) {
							throw new ShapeOverlapException(shape, pos, shapeAtTarget);
						}
					} catch (ArrayIndexOutOfBoundsException e) {
						throw new ShapeOutOfBoundsException(shape, pos);
					}
				}
			}
		}
		return true;
	}

	public void placeBlock(PuzzleBlock block, Point pos) {
		PuzzleBlockShape shape = block.getShape();
		char[][] shapeData = shape.getBlocks();
		if (canPlace(block, pos))
			;
		// place actual block
		for (int y = 0; y < shapeData.length; y++) {
			for (int x = 0; x < shapeData[y].length; x++) {
				if (isShapeBlock(shapeData[y][x])) {
					grid[pos.y + y][pos.x + x] = block;
				}
			}
		}
	}

	// just a single square, used when loading a save
	public void placeBlockPiece(PuzzleBlock block, Point pos) {
		grid[pos.x][pos.y] = block;
	}

	private boolean isShapeBlock(char shapeData) {
		return shapeData != '.';
	}

	public PuzzleBlock getPuzzleBlock(Point pos) {
		return grid[pos.y][pos.x];
	}

	//prints the model's grid to console. Only for debugging purposes.
	public void printGrid() {
		System.out.println();
		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				System.out.print((grid[y][x] != null ? grid[y][x] : '.') + " ");
			}
			System.out.println();
		}
	}

	// returns the grid with all the placed blocks in it
	public PuzzleBlock[][] getGridState() {
		return grid;
	}

	public void clearLine(int lineNumber, boolean horizontal) {
		if(clearLineSound.exists()) {
		clearLineSoundPlayer.playSound();
		}
		if (horizontal)
			for (int x = 0; x < 10; x++) {
				grid[lineNumber][x] = null;
			}
		if (!horizontal)
			for (int y = 0; y < 10; y++) {
				grid[y][lineNumber] = null;
			}
	}
}
