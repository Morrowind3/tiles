package core.model;

public class PuzzleBlock {
	private final PuzzleBlockShape shape;

	public PuzzleBlock(final PuzzleBlockShape shape) {

		this.shape = shape;
	}

	public PuzzleBlockShape getShape() {
		return shape;
	}

	@Override
	public String toString() {

		return Character.toString(shape.getIdentifier());

	}

}
