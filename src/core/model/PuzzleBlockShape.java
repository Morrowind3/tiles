package core.model;

import java.awt.*;

public class PuzzleBlockShape {
	private char[][] blocks;

	private Color colour;
	private char identifier;

	public PuzzleBlockShape(char[][] blocks, Color colour, char identifier) {
		this.blocks = blocks;
		this.colour = colour;
		this.identifier = identifier;
	}

	public char[][] getBlocks() {

		return blocks;
	}

	public int amountOfBlocks() {

		int amount = 0;
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				if (blocks[x][y] != '.') {
					amount++;
				}
			}
		}
		return amount;

	}

	public Color getColour() {

		return colour;
	}

	public char getIdentifier() {
		return identifier;
	}

}
