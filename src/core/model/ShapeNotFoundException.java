package core.model;


public class ShapeNotFoundException extends RuntimeException {
	public ShapeNotFoundException() {
		System.out.println("There is no shape in this dock!");
	}
}
