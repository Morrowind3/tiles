package core.model;

import java.awt.*;
public class ShapeOutOfBoundsException extends RuntimeException {
	public ShapeOutOfBoundsException(PuzzleBlockShape shape, Point pos) {
		super(String.format("Shape %s does not fit on position %d, %d", shape, pos.x, pos.y));
	}
}
