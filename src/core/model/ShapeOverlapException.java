package core.model;

import java.awt.*;

public class ShapeOverlapException extends RuntimeException {
	public ShapeOverlapException(PuzzleBlockShape shape, Point pos, PuzzleBlock shapeAtTarget) {
		super(String.format("Shape %s can not be placed as position %d, %d is already occupied by %s.", shape, pos.x,
				pos.y, shapeAtTarget));
	}
}
