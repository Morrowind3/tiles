package core.utility;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class ComponentListenerAdapter implements ComponentListener {
	@Override
	public void componentResized(ComponentEvent e) {

	}

	@Override
	public void componentMoved(ComponentEvent e) {

	}

	@Override
	public void componentShown(ComponentEvent e) {

	}

	@Override
	public void componentHidden(ComponentEvent e) {

	}
}
