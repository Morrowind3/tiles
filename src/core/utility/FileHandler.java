package core.utility;

import java.io.*;
import java.util.ArrayList;

import client.controller.GameController;
import core.model.Game;

public class FileHandler {

	private BufferedReader reader;

	private ArrayList<String> fileLines = new ArrayList<String>();

	public FileHandler(File file) {

		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			return;
		}

		try {
			String line = reader.readLine();
			while (line != null) {
				fileLines.add(line);
				line = reader.readLine();

			}
			reader.close();
		} catch (IOException ex) {
			System.out.println("File not found!");
			return;
		}

	}

	public String getLine(int lineNumber) {

		return fileLines.get(lineNumber);

	}

}