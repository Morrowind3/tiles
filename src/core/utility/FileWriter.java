package core.utility;

import java.io.*;

public class FileWriter {

	private BufferedWriter writer;

	public FileWriter(String file) {

		try {
			writer = new BufferedWriter(new java.io.FileWriter(file));
		} catch (IOException e) {
			System.out.println("I/O exception!");
		}
	}

	public void writeLine(String stringToWrite) {
		try {

			writer.write(stringToWrite, 0, stringToWrite.length());
			writer.flush();
		} catch (IOException e) {
			System.out.println("writing failed!");
			e.printStackTrace();

		}

	}

	public void newLine() {
		try {
			writer.newLine();
		} catch (IOException e) {
			System.out.println("Somehow, printing a new line failed");
		}
	}
}
